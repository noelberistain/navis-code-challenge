import React from "react";
import { CardContent as MUICardContent } from "@material-ui/core";

export const CardContent = (props) => <MUICardContent {...props} />;

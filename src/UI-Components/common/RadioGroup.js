import React from "react";
import { RadioGroup as MUIRadioGroup } from "@material-ui/core";

export const RadioGroup = (props) => <MUIRadioGroup {...props} />;

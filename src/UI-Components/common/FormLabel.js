import React from "react";
import { FormLabel as MUIFormLabel } from "@material-ui/core";

export const FormLabel = (props) => <MUIFormLabel {...props} />;

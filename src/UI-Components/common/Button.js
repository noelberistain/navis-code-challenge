import React from "react";
import { Button as MUIButton } from "@material-ui/core";

export const Button = ({
  color = "default",
  isDisabled = false,
  label,
  size,
  variant = "contained",
  handleClick,
}) => {
  return (
    <MUIButton
      color={color}
      disabled={isDisabled}
      size={size}
      variant={variant}
      onClick={handleClick}
    >
      {label}
    </MUIButton>
  );
};

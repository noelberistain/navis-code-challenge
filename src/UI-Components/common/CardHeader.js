import React from "react";
import { CardHeader as MUICardHeader } from "@material-ui/core";

export const CardHeader = (props) => <MUICardHeader {...props} />;

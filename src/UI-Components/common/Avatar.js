import React from "react";
import { Avatar as MUIAvatar } from "@material-ui/core";

export const Avatar = (props) => {
  const { iconSrc, ...rest } = props;
  return <MUIAvatar src={iconSrc} {...rest} />;
};

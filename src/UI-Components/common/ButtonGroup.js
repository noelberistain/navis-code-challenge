import React from "react";
import { ButtonGroup as MUIButtonGroup } from "@material-ui/core";

export const ButtonGroup = (props) => <MUIButtonGroup {...props} />;

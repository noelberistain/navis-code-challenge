import React from "react";
import { TextField as MUITextField } from "@material-ui/core";

export const TextField = (props) => {
  const { error, helpText, label, size, handleChange, value, ...rest } = props;

  return (
    <MUITextField
      error={error}
      helperText={helpText}
      label={label}
      size={size}
      value={value}
      variant="outlined"
      onChange={handleChange}
      {...rest}
    />
  );
};

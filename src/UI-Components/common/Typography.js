import React from "react";
import { Typography as MUITypography } from "@material-ui/core";

export const Typography = (props) => <MUITypography {...props} />;

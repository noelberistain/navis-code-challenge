import React, { useState } from "react";
import { makeStyles } from "@material-ui/core";
import { Button, FormControlLabel, Grid, TextField } from "../common";
import {
  weatherByZipCode,
  weatherByZipCodeWithForecast,
} from "../../utils/weatherAPI";
import { SelectCity } from "../weather/SelectCity";
import { ForecastDays } from "../weather/ForecastDays";

const useStyles = makeStyles((theme) => ({
  items: { margin: theme.spacing(2) },
}));

// in case we want to add more country codes just need to add it to the array
const countryCodesList = ["us", "mx"];

export const Header = ({
  query,
  setWeather,
  forecastDay,
  forecastDayChange,
  handleQueryChange,
}) => {
  const classes = useStyles();

  const [country, setCountry] = useState("us");

  const weatherZipCode = async function () {
    const results = await weatherByZipCode({
      zipCode: query,
      options: country,
    });
    setWeather(results);
  };

  const weatherForDays = async function () {
    const results = await weatherByZipCodeWithForecast({
      zipCode: query,
      options: country,
      days: forecastDay,
    });
    setWeather(results);
  };

  const [error, setError] = useState(false);
  const reg = /^\d+$/;

  const handleSubmit = (event) => {
    event.preventDefault();
    // simple validation knowing a zip code needs 5 digits
    if (query.trim() !== "" && query.length === 5 && reg.test(query)) {
      if (forecastDay > 1) {
        weatherForDays();
      } else {
        weatherZipCode();
      }
      // after calling if there was an error, we need to set error to false again.
      if (error) {
        setError(false);
      }
    } else {
      setError(true);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <Grid container>
        <Grid item xs={12} md={6}>
          <FormControlLabel
            className={classes.items}
            control={
              <TextField
                error={error}
                helpText={error && `Type a valid Zip Code`}
                label="Zip Code"
                value={query}
                size="small"
                onChange={handleQueryChange}
              />
            }
          />
          <FormControlLabel
            className={classes.items}
            control={
              <SelectCity
                country={country}
                countryCodes={countryCodesList}
                setCountry={setCountry}
              />
            }
          />
        </Grid>
        <Grid item xs={12} md={6} className={classes.root}>
          <FormControlLabel
            className={classes.items}
            control={
              <ForecastDays
                forecastDay={forecastDay}
                handleChange={forecastDayChange}
              />
            }
          />
          <Button
            className={classes.items}
            color="primary"
            isDisabled={false}
            label="Search"
            handleClick={handleSubmit}
            size="medium"
          />
        </Grid>
      </Grid>
    </form>
  );
};

import { makeStyles } from "@material-ui/core";
import React, { useState } from "react";
import { Grid } from "../common";
import { Header } from "../header";
import { WeatherComponent } from "./WeatherComponent";

const useStyles = makeStyles((theme) => ({
  root: { border: "thin solid black" },
}));

export const WeatherContainer = () => {
  const classes = useStyles();
  const [weatherInfo, setWeatherInfo] = useState({});

  const [forecastDay, setForeCastDay] = React.useState(1);
  const [query, setQuery] = useState("");
  const handleQueryChange = (event) => {
    setQuery(event.target.value);
  };

  const forecastDayChange = (event) => {
    setForeCastDay(Number(event.target.value));
    setWeatherInfo({});
  };

  return (
    <Grid className={classes.root} container direction="column">
      <Header
        query={query}
        setWeather={setWeatherInfo}
        forecastDay={forecastDay}
        forecastDayChange={forecastDayChange}
        handleQueryChange={handleQueryChange}
        setQuery={setQuery}
      />
      <WeatherComponent weatherInfo={weatherInfo} forecastDay={forecastDay} />
    </Grid>
  );
};

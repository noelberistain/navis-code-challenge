import React from "react";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Typography,
} from "../common";

import { makeStyles } from "@material-ui/core";
import { getIcon } from "../../utils/weatherAPI/assets/icons/icons";

const useStyles = makeStyles((theme) => ({
  // creates a useStyles hook for functional components
  root: {
    margin: theme.spacing(2),
  },
  minTemp: { color: "blue" },
  maxTemp: { color: "tomato" },
}));

const WeatherCardItem = ({
  forecastDay,
  cityName,
  country,
  temp,
  minTemp,
  maxTemp,
  description,
  icon,
  day,
  month,
  year,
}) => {
  // get classes obj from custom useStyles hook
  const classes = useStyles();

  const date =
    forecastDay > 1 ? new Date(year, month, day).toDateString() : "Today";

  return (
    <Grid item md={3} sm={5} xs={10} className={classes.root}>
      <Card>
        <CardHeader
          avatar={
            <Avatar iconSrc={`${getIcon(icon)}`} className={classes.avatar} />
          }
          title={`${cityName}, ${country || ""} | ${date}`}
        />
        <CardContent>
          <Typography
            variant="h5"
            color="textSecondary"
            style={{ textAlign: "center", color: "green" }}
          >
            {`${temp} °F`}
          </Typography>
          <Typography
            variant="h6"
            color="textSecondary"
            style={{ textAlign: "center" }}
          >
            {`${description}`}
          </Typography>

          <Box>
            <Typography variant="body2" color="textSecondary">
              Forecast
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              className={classes.minTemp}
            >
              {`: ${minTemp} °F /`}
            </Typography>

            <Typography
              variant="body2"
              color="textSecondary"
              className={classes.maxTemp}
            >
              {`${maxTemp} °F`}
            </Typography>
          </Box>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default WeatherCardItem;

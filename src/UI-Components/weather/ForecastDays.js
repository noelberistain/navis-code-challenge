import React from "react";
import {
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
} from "../common";

export const ForecastDays = ({ forecastDay, handleChange }) => {
  return (
    <Grid item>
      <FormLabel>Consult next days:</FormLabel>
      <RadioGroup
        name="forecast days"
        aria-label="forecast days"
        value={forecastDay.toString()}
        onChange={handleChange}
        row
      >
        {[1, 2, 3, 4].map((value) => (
          <FormControlLabel
            key={value}
            value={value.toString()}
            control={<Radio />}
            label={value.toString()}
          />
        ))}
      </RadioGroup>
    </Grid>
  );
};

import React from "react";
import { Button, ButtonGroup } from "../common/";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
  },
}));

export const SelectCity = ({ country, countryCodes, setCountry }) => {
  const classes = useStyles();

  const handleClick = (event, countryCode) => {
    event.preventDefault();
    setCountry(countryCode);
  };
  const countryCodeButtons =
    countryCodes.length > 0
      ? countryCodes.map((countryCode) => (
          <Button
            aria-label={countryCode}
            color={countryCode === country && "secondary"}
            key={countryCode.toString()}
            label={countryCode}
            size="medium"
            handleClick={(e) => handleClick(e, countryCode)}
          />
        ))
      : null;
  return (
    <div className={classes.root}>
      <ButtonGroup color="primary" aria-label="country code buttons">
        {countryCodeButtons}
      </ButtonGroup>
    </div>
  );
};

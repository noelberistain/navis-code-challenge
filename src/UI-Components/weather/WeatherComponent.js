import React, { Suspense } from "react";
import { Box, Grid } from "../common";
import { makeStyles } from "@material-ui/core";
import { Loading } from "../Loading/Loading";

// Dinamically importing a component when it is actually needed to imported
const WeatherCardList = React.lazy(() => import("./WeatherCardList"));

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
}));
export const WeatherComponent = ({ weatherInfo, forecastDay }) => {
  let { responseCode, message = "", list } = weatherInfo;

  const classes = useStyles();
  return (
    <Grid className={classes.root} container justify="center">
      {responseCode === "404" && (
        <Box>
          <h3>Something went wrong, message: {message}</h3>
        </Box>
      )}
      {responseCode === "200" ? (
        <>
          <Suspense fallback={<Loading />}>
            <WeatherCardList list={list} forecastDay={forecastDay} />
          </Suspense>
        </>
      ) : (
        <Box>
          <h3>Type a Zip Code</h3>
        </Box>
      )}
    </Grid>
  );
};

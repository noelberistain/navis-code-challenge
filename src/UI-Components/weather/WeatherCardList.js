import React from "react";
import WeatherCardItem from "./WeatherCardItem";

const WeatherCardList = ({ list, forecastDay }) => {
  const cardlist =
    list.length > 0
      ? list.map((item) => (
          <WeatherCardItem
            forecastDay={forecastDay}
            key={item.temp}
            cityName={item.cityName}
            country={item.country}
            temp={item.temp}
            description={item.description}
            maxTemp={item.maxTemp}
            minTemp={item.minTemp}
            icon={item.icon}
            day={item.day}
            month={item.month}
            year={item.year}
          />
        ))
      : "nothing on list";

  return <>{cardlist}</>;
};

export default WeatherCardList;

import { config } from "../config";

let baseItem = {
  cityName: "",
  country: "",
  description: "",
  maxTemp: "",
  minTemp: "",
  temp: "",
  icon: "",
};
const baseResponse = {
  responseCode: null,
  list: [baseItem],
};

//searchType should be 'byZipCode'
//options, if it's not defined it'll be seted to its default: 'us'.
export const weatherByZipCode = async function ({ zipCode, options = "us" }) {
  const url = `https://api.openweathermap.org/data/2.5/weather?zip=${zipCode},${options}&units=imperial&appid=${config.api.key}`;
  try {
    const { results } = await call(url);
    if (results.cod === "404") {
      const response = {};
      response.responseCode = results.cod;
      response.message = results.message;
      return response;
    }
    const {
      cod,
      name,
      sys: { country },
      weather,
      main: { temp, temp_min, temp_max },
    } = results;
    const { description, icon } = weather[0];

    baseResponse.responseCode = String(cod);
    baseItem.cityName = name;
    baseItem.country = country;
    baseItem.description = description;
    baseItem.icon = icon;
    baseItem.temp = temp;
    baseItem.minTemp = temp_min;
    baseItem.maxTemp = temp_max;

    return baseResponse;
  } catch (error) {
    return error;
  }
};

export const weatherByZipCodeWithForecast = async function ({
  zipCode,
  options = "us",
  days,
}) {
  const url = `https://api.openweathermap.org/data/2.5/forecast?zip=${zipCode},${options}&units=imperial&appid=${config.api.key}`;
  try {
    const { results } = await call(url);
    if (results.cod === "404") {
      const response = {};
      response.responseCode = results.cod;
      response.message = results.message;
      return response;
    }
    const {
      cod,
      city: { name, country },
      list,
    } = results;

    baseResponse.responseCode = String(cod);
    const temp = filterListByGivenDays(list, days);
    // Once we have the correct number of items saved on 'temp' constant
    // we fill the baseResponse accordingly for returning
    baseResponse.list = temp.map((item) => {
      baseItem = {};
      const { weather, dt_txt: date } = item;
      const { description, icon } = weather[0];
      const aux = date.split(" ")[0];
      const [year, month, day] = aux.split("-").map((item) => Number(item));

      baseItem.cityName = name;
      baseItem.country = country;
      baseItem.description = description;
      baseItem.icon = icon;
      baseItem.temp = item.main.temp;
      baseItem.minTemp = item.main.temp_min;
      baseItem.maxTemp = item.main.temp_max;
      baseItem.year = year;
      baseItem.month = month;
      baseItem.day = day;

      return baseItem;
    });

    return baseResponse;
  } catch (error) {
    return error;
  }
};

// receives the response list and returns tne number of Days specified from it
// simple filtration; by now, the exact data won't be the one returned
const filterListByGivenDays = (items, limit) => {
  const aux = items.filter((item) => {
    const itemTime = item.dt_txt.split(" ")[1];
    const hrValue = itemTime.split(":")[0];
    return hrValue === "00";
  });
  return aux.splice(0, limit);
};

const call = (url) =>
  fetch(url)
    .then((res) => {
      if (res.status === 200) {
        console.log("Success :" + res.statusText); // check for status 200
      } else if (res.status === 400) {
        console.log("res.status", res.status);
        console.log(JSON.stringify(res.body.json())); // check for status 400
      }
      return res.json(); // return readable stream parsed to json
    })
    .then((res) => {
      let results = res;
      return { results };
    })
    .catch((error) => {
      console.log("error", error);
      return { error }; // rejected fetch handling
    });

import React from "react";
import "./App.css";
import { WeatherContainer } from "./UI-Components";
import { Box } from "./UI-Components/common";

function App() {
  return (
    <Box className="App">
      <WeatherContainer />
    </Box>
  );
}

export default App;
